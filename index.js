const http = require('http')
const url = require('url')
const WebSocketServer = require('websocket').server;
const fs = require('fs');
const HTTP_PORT = 8080;
const WS_PORT = 4040;

// ----   SERVER HTTP   ----- 
var HTTPServer = http.createServer(function (req, res) {
    let parsedUrl = url.parse(req.url, true);
    //let query = parsedUrl.query; pas utilisé
    res.writeHead(200, {
        'Content-Type': 'text/html',
    })
    let chemin =  "./client/index.html"
    switch (parsedUrl.path){
        case "/":
            chemin = "./client/index.html"
        break
        case "/style.css":
            chemin = "./client/style.css"
            res.writeHead(200, {
                "content-type": "text/css;charset=UTF-8"
            })
        break
        case "/script.js":
            chemin = "./client/script.js"
        break
        default:
            chemin = "./client/index.html"

    }
    fs.readFile( chemin, (err, data) => {
        if (err) {
            res.writeHead(404)
            res.write("Page introuvable!")
        }
        else {          
            res.write(data)
            res.end()
        }
    })
})
HTTPServer.listen(HTTP_PORT, function () {
    // console.log("HTTP Server listening on: http://localhost:%s", HTTP_PORT);
});

// ----   SERVEUR WEB SOCKET   ----- 
var serverWS = http.createServer(function (request, response) {
    // console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
serverWS.listen(WS_PORT, function () {
    // console.log((new Date()) + ' Server is listening on port 4040');
});

// Mise en place du WebSocket
wsServer = new WebSocketServer({
    httpServer: serverWS,
    autoAcceptConnections: false
});
function originIsAllowed(origin) {
    return true;
}

//Ecoute du WebSocket
let tabListeUser = [];
wsServer.on('request', function (request) {
    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
        request.reject();
        // console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
    }

    var connection = request.accept('echo-protocol', request.origin);
    //Stockage des connexion courantes
    if (!tabListeUser[connection]) {
        tabListeUser.push(connection);
    }
    const index = tabListeUser.indexOf(connection);
    connection.on('message', function (message) {
        if (message.type === 'utf8') {
            broadcast(message.utf8Data)
            // broadcast(message.utf8Data, (JSON.parse(message.utf8Data).message == 'ATTAQUE')?tabListeUser[index]:null)
            // connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            // console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function (reasonCode, description) {
        if( index > -1){
            tabListeUser.splice(index,1);
        }
        // console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});

function broadcast(msg, userEscape = null) {
    tabListeUser.forEach(function each(client) {
        if (userEscape == null || userEscape != client){
            client.send(msg);
        }
        // console.log(client)
    });
};

