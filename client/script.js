let pseudo ;
let pseudoAncien;
var WSocket =  new WebSocket('ws://localhost:4040','echo-protocol');
//Listener DOM
document.getElementById('mybutton').addEventListener('click',(event)=>{
    let message = document.getElementById('myinput').value
    pseudo = document.getElementById("pseudo").value;
    if(pseudo != pseudoAncien){
        var noeud = document.createElement('div');
        noeud.innerText = "Hey !! salut  " + pseudo;
        noeud.classList.add('pseudo','message','presentation');
        var chatsection = document.getElementById('message');
        chatsection.insertBefore(noeud,chatsection.firstElementChild );
    }
    pseudoAncien = pseudo;
    let data = JSON.stringify({'message' : message, 'pseudo' : pseudo})
    WSocket.send(data);
    document.getElementById('myinput').value = ""
    event.preventDefault
})
//Masque zone de message si pas de pseudo
document.getElementById("pseudo").addEventListener('keyup',(event) =>{
    if(event.target.value == ""){
        document.getElementsByTagName('container')[0].classList.add('hidden');
    }else {
        document.getElementsByTagName('container')[0].classList.remove('hidden');
    }
})
//Masque bouton d'envoi si pas de message
document.getElementById("myinput").addEventListener('keyup',(event) =>{
    if(event.target.value == ""){
        document.getElementById('mybutton').classList.add('hidden');
    }else {
        document.getElementById('mybutton').classList.remove('hidden');
    }
    if('Enter' == event.key){
        eventclick = new Event('click');
        document.getElementById('mybutton').dispatchEvent(eventclick)
        event.preventDefault
    }
})
document.getElementById("action").addEventListener('click',(event) =>{
    if (event.target.value == 'ATTAQUE' ){
        let data = JSON.stringify({'message' : "ATTAQUE", 'pseudo' : pseudo})
        WSocket.send(data);
    } else {
        document.getElementsByClassName('container-chat')[0].classList.remove('attaque')
        document.getElementById('action').value = 'ATTAQUE';
    }
})
//Listener WSSOcket
WSocket.onmessage = (event) => {
    pseudo = document.getElementById("pseudo").value;
    var noeud = document.createElement('div');
    data = JSON.parse(event.data)
    switch (data.pseudo == pseudo){
        case true:
            noeud.classList.add('pseudo','message');
            pseudo == "" ? pseudo = "inconnu" : null
            noeud.innerText = data.message;
            break
        case false:
            noeud.classList.add('interlocuteur','message');
            data.pseudo == "" ? data.pseudo = "inconnu" : data.pseudo
            noeud.innerHTML = `<i>message de ${data.pseudo} : </i>` + data.message;
            break
    }
    var chatsection = document.getElementById('message');
    chatsection.insertBefore(noeud,chatsection.firstElementChild );
    if(data.message == 'ATTAQUE' ){
        if (data.pseudo != pseudo){
            document.getElementsByClassName('container-chat')[0].classList.add('attaque')
            document.getElementById('action').value = 'DEFENSE';
        } else {
            noeud.innerText = "OHHH !!   " + pseudo + " vous avez ATTAQUÉ";
            noeud.classList.add('pseudo','message','presentation');
            var chatsection = document.getElementById('message');
            chatsection.insertBefore(noeud,chatsection.firstElementChild );
        }
    }
  };